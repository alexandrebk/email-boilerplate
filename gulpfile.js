const gulp = require('gulp')
const pug  = require('gulp-pug')
const stylus  = require('gulp-stylus')
const plumber = require('gulp-plumber')
const inlineCss   = require('gulp-inline-css')
const browserSync = require('browser-sync')

gulp.task('pug', function buildHTML() {
	return gulp.src('src/*.pug')
		.pipe(plumber())
		.pipe(pug({pretty:true}))
		.pipe(plumber.stop())
		.pipe(gulp.dest('./dist/'))
})

gulp.task('inlineCSS', () => {
	return gulp.src('dist/*.html')
		.pipe(inlineCss())
		.pipe(gulp.dest('./dist/'))
})

gulp.task('stylus', () => {
	return gulp.src('src/css/main.styl')
		.pipe(stylus({compress: true}))
		.pipe(gulp.dest('./dist/css'))
})

gulp.task('server', () => {
    var files = [
        './dist/**/*'
    ]
    browserSync.init( files, {
        server: {
            baseDir: "./dist"
        }
    })
})

gulp.task('watch', () => {
    gulp.watch('src/**/*.pug', ['pug'])
    gulp.watch('src/css/**/*.styl', ['stylus'])
    gulp.watch('dist/**/*.html', ['inlineCSS'])
})

gulp.task('build', ['pug', 'stylus'])